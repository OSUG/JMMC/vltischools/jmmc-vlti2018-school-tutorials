# Recipes to build a virtual box machine using vagrant and ansible for VLTI 2018 School

http://www.european-interferometry.eu/training/2018-school

## Virtual Machine setup

To start buiding, run:
```bash
vagrant up
```

If you modify the ansible playbook 'school-playbook.yml' or roles recepices and want to apply changes, run:
```bash
vagrant provision
```

Virtual machine details are stored into the 'Vagrantfile'. To export the built machine, run:
```bash
vagrant package
```


